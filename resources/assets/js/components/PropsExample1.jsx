import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

class Badge1 extends React.Component {
    render() {
        return (
            <div>
                <h2>Method 1</h2>
                <img src={this.props.img} style={{width: '100%'}} />
                <h3>Name: {this.props.name} </h3>
                <h4>username: {this.props.username}</h4>
            </div>
        )
    }
}

Badge1.propTypes = {
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired
}

ReactDOM.render(
    <Badge1
        name='Tyler McGinnis'
        username='tylermcginnis'
        // img={{image :'https://avatars0.githubusercontent.com/u/2933430?v=3&s=460' }}/>,
        img='https://avatars0.githubusercontent.com/u/2933430?v=3&s=460'/>,
    document.getElementById('props_example_1')
);

export default Badge1;