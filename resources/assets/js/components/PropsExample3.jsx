import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Avatar extends React.Component {
    render() {
        return (
            <img src={this.props.img} style={{width: '100%'}} />
        )
    }
}

class Label extends React.Component {
    render() {
        return (
            <h3>Name: {this.props.name}</h3>
        )
    }
}

class ScreenName extends React.Component {
    render() {
        return (
            <h4>Username: {this.props.username}</h4>
        )
    }
}

class Badge3 extends React.Component {
    render() {
        return (
            <div>
                <h2>Method 3</h2>
                <Avatar img={this.props.user.img}/>
                <Label name={this.props.user.name}/>
                <ScreenName username={this.props.user.username}/>
            </div>
        )
    }
}

ReactDOM.render(
    <Badge3 user={{
        name: 'Tyler McGinnis',
        img: 'https://avatars0.githubusercontent.com/u/2933430?v=3&s=460',
        username: 'tylermcginnis'
    }} />,
    document.getElementById('props_example_3')
);

export default Badge3;