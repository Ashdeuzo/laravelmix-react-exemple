import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Example extends Component {
    render() {
        return (
            <div>
                <h3>Hey! it's working ! GG {this.props.name} :D</h3>
            </div>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Example name="Hazem "/>, document.getElementById('example'));
}

export default Example;