import React, { Component } from 'react';
import ReactDOM from 'react-dom';

var USER_DATA = {
    name: 'Tyler McGinnis',
    img: 'https://avatars0.githubusercontent.com/u/2933430?v=3&s=460',
    username: 'tylermcginnis'
}

class Badge2 extends React.Component {
    render() {
        return (
            <div>
                <h2>Method 2</h2>
                <img src={this.props.user.img} style={{width: '100%'}} />
                <h3>Name: {this.props.user.name} </h3>
                <h4>username: {this.props.user.username}</h4>
            </div>
        )
    }
}

ReactDOM.render(
    <Badge2 user={USER_DATA}/>,
    document.getElementById('props_example_2')
);

export default Badge2;