@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h1>Simple Example : </h1>
                    <div id="example"></div>
                    <hr/>
                    <h1>Props Example : </h1>
                    <div class="row">
                        <div class="col-md-4">
                            <div id="props_example_1"></div>
                        </div>
                        <div class="col-md-4">
                            <div id="props_example_2"></div>
                        </div>
                        <div class="col-md-4">
                            <div id="props_example_3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/reactExamples.js') }}"></script>
@endsection
